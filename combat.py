#!/usr/bin/python
# -*- coding: utf8 -*-

import sys
from random import randint

from regles import base, evo
from profils import *

def norm (carac) :
	if len(carac)<3 :
		carac+=" "*(3-len(carac))
	return carac

def creer_joueur(profil) :
	joueur = {}
	joueur['cc']  = profil['cc']  
	joueur['ct']  = profil['ct']  
	joueur['ag']  = profil['ag']  
	joueur['f']   = profil['f']   
	joueur['fm']  = profil['fm']  
	joueur['m']   = profil['m']   
	joueur['pm']  = profil['pm']  
	joueur['rm']  = profil['rm']  
	joueur['e']   = profil['e']   
	joueur['pv']  = profil['pv']  
	joueur['pf']  = profil['pf']  
	joueur['r']   = profil['r']   
	joueur['rf']  = profil['rf']  
	joueur['pa']  = profil['pa']  
	joueur['mv']  = profil['mv']  
	joueur['p']   = profil['p']   
	joueur['pm_r']= profil['pm']
	joueur['pv_r']= profil['pv']
	joueur['pf_r']= profil['pf']
	joueur['pa_r']= profil['pa']
	joueur['mv_r']= profil['mv']
	return joueur

def tir (joueur1, item1, maitrise1, joueur2, item2, maitrise2, portee_max, bouclier, silent) :
	#On calcule si on tir à l'agilité ou non
	ct_base = joueur1["ct"]*item1["ct_mult"]+item1["ct"]+(maitrise1*15/250)
	ct_ag = ((joueur1["ct"]*item1["ct_mult"]+item1["ct"]+(maitrise1*15/250))*0.7 + (joueur1["ag"]*item1["ag_mult"]+item1["ag"])*0.5) 
	if ct_base < ct_ag :
		ct_eff = ct_ag
		if not silent :
			print "CT de base : "+norm(str(ct_base))+", CT avec Ag : "+norm(str(ct_ag))+" =>  tir agile"
	else :
		ct_eff = ct_base
		if not silent :
			print "CT de base : "+norm(str(ct_base))+", CT avec Ag : "+norm(str(ct_ag))+" =>  tir standard"
	#on calcule le malus de fatigue
	fatigue_malus = (joueur1["pf"]-joueur1['pf_r'])*20/joueur1['pf']
	if not silent :
		print "Malus de fatigue de "+str(fatigue_malus)
	fatigue_bonus = (joueur2["pf"]-joueur2['pf_r'])*40/joueur2['pf']
	if not silent :
		print "Bonus de fatigue de "+str(fatigue_bonus)
	#On ajoute le malus de portée
	if portee_max :
		portee_malus = 10
		if not silent :
			print "Portée maximale, malus de 10"
	else :
		portee_malus = 0
	#On calcul le seuil
	attaque_seuil = ct_eff-fatigue_malus+fatigue_bonus-portee_malus
	if not silent :
		print "Seuil à "+str(attaque_seuil)
	#On fait le jet
	de1 = randint(1,30)
	de2 = randint(1,30)
	de3 = randint(1,30)
	de4 = randint(1,30)
	attaque_jet = de1+de2+de3+de4
	if not silent :
		print "Jet à "+str(attaque_jet)
	if attaque_jet > attaque_seuil :
		if not silent :
			print "Tir raté"
		return -2
	#On soustrait la marge
	marge_malus = attaque_jet - attaque_seuil
	if not silent :
		print "Marge de "+str(marge_malus)
	#On fait le calcul du type de défense utilisé
	def_ag = joueur2["ag"]*item2["ag_mult"]+item2["ag"]
	def_cc_ag = (joueur2["ag"]*item2['ag_mult']+item2["ag"])*0.6+(joueur2["cc"]*item2["cc_mult"]+item2["cc"]+(maitrise2*15/250))*0.7
	def_cc = joueur2["cc"]*item2["cc_mult"]+item2["cc"]+(maitrise2*15/250)
	if bouclier :
		if not silent :
			print "Bouclier présent"
		def_eff = max(def_ag, def_cc_ag, def_cc)
	else : #Seule l'Ag est possible
		def_eff = def_ag
	if not silent :
		if def_eff == def_ag :
			print "Parade à l'ag pure"
		elif def_eff == def_cc_ag :
			print 'Parade à la "CC agile"'	
		elif def_eff == def_cc :
			print "Parade à la CC"
		else :
			print "Pas normal"
			sys.exit(1)
	defense_seuil = def_eff+marge_malus
	if not silent :
		print "Seuil à "+str(defense_seuil)
	de1 = randint(1,30)
	de2 = randint(1,30)
	de3 = randint(1,30)
	de4 = randint(1,30)
	defense_jet = de1+de2+de3+de4
	if not silent :
		print "Jet à "+str(defense_jet)
	if defense_jet <= defense_seuil  :
		if not silent :
			print "Tir paré"
		return -1
	#Jet d'armure
	puissance_jet = joueur1["f"]*item1["f_mult"]+item1["f"]+randint(1,3)
	armure_jet = joueur2["e"]*item2["e_mult"]+item2["e"] #On ignore le côté aléatoire
	#Calcul du nombre de dégats
	degats = puissance_jet - armure_jet
	if not silent :
		print "Dégats = "+str(degats)
	return degats

def frappe (joueur1, item1, maitrise1, joueur2, item2, maitrise2, silent) :
	#On calcule si on frappe à l'agilité ou non
	cc_base = joueur1["cc"]*item1["cc_mult"]+item1["cc"]+(maitrise1*15/250)
	cc_ag = ((joueur1["cc"]*item1["cc_mult"]+item1["cc"]+(maitrise1*15/250))*0.7 + (joueur1["ag"]*item1["ag_mult"]+item1["ag"])*0.6) 
	if cc_base < cc_ag :
		cc_eff = cc_ag
		if not silent :
			print "CC de base : "+norm(str(cc_base))+", CC avec Ag : "+norm(str(cc_ag))+" =>  frappe agile"
	else :
		cc_eff = cc_base
		if not silent :
			print "CC de base : "+norm(str(cc_base))+", CC avec Ag : "+norm(str(cc_ag))+" =>  frappe standard"
	#on calcule le malus de fatigue
	fatigue_malus = (joueur1["pf"]-joueur1['pf_r'])*20/joueur1['pf']
	if not silent :
		print "Malus de fatigue de "+str(fatigue_malus)
	fatigue_bonus = (joueur2["pf"]-joueur2['pf_r'])*40/joueur2['pf']
	if not silent :
		print "Bonus de fatigue de "+str(fatigue_bonus)
	#On calcul le seuil
	attaque_seuil = cc_eff-fatigue_malus+fatigue_bonus
	if not silent :
		print "Seuil à "+str(attaque_seuil)
	#On fait le jet
	de1 = randint(1,30)
	de2 = randint(1,30)
	de3 = randint(1,30)
	de4 = randint(1,30)
	attaque_jet = de1+de2+de3+de4
	if not silent :
		print "Jet à "+str(attaque_jet)
	if attaque_jet > attaque_seuil :
		if not silent :
			print "Frappe raté"
		return -2
	#On soustrait la marge
	marge_malus = attaque_jet - attaque_seuil
	if not silent :
		print "Marge de "+str(marge_malus)
	def_ag = joueur2["ag"]*item2["ag_mult"]+item2["ag"]
	def_cc_ag = (joueur2["ag"]*item2['ag_mult']+item2["ag"])*0.6+(joueur2["cc"]*item2["cc_mult"]+item2["cc"]+(maitrise2*15/250))*0.7
	def_cc = joueur2["cc"]*item2["cc_mult"]+item2["cc"]+(maitrise2*15/250)
	def_eff = max(def_ag, def_cc_ag, def_cc)
	if not silent :
		if def_eff == def_ag :
			print "Parade à l'ag pure"
		elif def_eff == def_cc_ag :
			print 'Parade à la "CC agile"'	
		elif def_eff == def_cc :
			print "Parade à la CC"
		else :
			print "Pas normal"
			sys.exit(1)
	defense_seuil = def_eff+marge_malus
	if not silent :
		print "Seuil à "+str(defense_seuil)
	de1 = randint(1,30)
	de2 = randint(1,30)
	de3 = randint(1,30)
	de4 = randint(1,30)
	defense_jet = de1+de2+de3+de4
	if not silent :
		print "Jet à "+str(defense_jet)
	if defense_jet <= defense_seuil  :
		if not silent :
			print "Frappe paré"
		return -1
	#Jet d'armure
	puissance_jet = joueur1["f"]*item1["f_mult"]+item1["f"]+randint(1,3)
	armure_jet = joueur2["e"]*item2["e_mult"]+item2["e"] #On ignore le côté aléatoire
	#Calcul du nombre de dégats
	degats = puissance_jet - armure_jet
	if not silent :
		print "Dégats = "+str(degats)
	return degats

def combat (joueur1,item1,type1,portee1,joueur2,item2,type2,portee2) :
	perso1=creer_joueur(eval(joueur1))
	perso2=creer_joueur(eval(joueur2))
	print perso2["cc"]

	#On commence par dire que le joueur avec la plus grande portée commence avec un bonus de PA

	#Choix du joueur commençant
	if randint(1,2) == 1 :
		print joueur1+" commence"
	else :
		print joueur2+" commence"

def test1 () :
	from profils import morane, gared
	from items import items_morane, items_gared
	joueur_morane = creer_joueur(morane)
	joueur_gared = creer_joueur(gared)
	#Test de parade
	tests=10000
	rates=0
	pares=0
	nuls=0
	xp_loss=0
	for i in range(0,tests):
		#degats=frappe(joueur_gared,items_gared,125,joueur_morane,items_morane,0, True)
		degats=tir(joueur_morane,items_morane,125,joueur_gared,items_gared,125, False, False, True)
		xp_loss+=1
		if degats == 0 :
			nuls+=1
		elif degats == -1 :
			pares+=1
		elif degats == -2 :
			rates+=1
		elif degats > 0 :
			xp_loss-=1
	print "Ratés : "+str(rates/(tests/100.))
	print "Parés : "+str(pares/(tests/100.))
	print "Nuls : "+str(nuls/(tests/100.))
	print "Total inutiles : "+str(xp_loss/(tests/100.))

def test2 () :
	from profils import morane, gared
	from items import items_morane, items_gared
	joueur_morane = creer_joueur(morane)
	joueur_gared = creer_joueur(gared)
	#frappe(joueur_gared,items_gared,125,joueur_morane,items_morane,0, False)
	tir(joueur_morane,items_morane,125,joueur_gared,items_gared,125, False, False, False)
	

def main () :
	#test1()
	#test2()
	combat("morane","items_morane","tir",3,"gared","items_gared","frappe",0)

main()
