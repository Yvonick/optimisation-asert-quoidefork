#!/usr/bin/python
# -*- coding: utf8 -*-

import sys

from regles import base, evo
from profils import *

def cout(race,carac,lvl,silent) :
	lvl = int(lvl)
	coeff = 1
	if race=="agar" and carac in agar :
		initial = agar[carac]			
	elif race=="nain" and carac in nain :
		initial = nain[carac]
	elif race=="he" and carac in he :
		initial = he[carac]
	elif race=="talien" and carac in talien :
		initial = talien[carac]
	elif race=="moy" and carac in moy :
		initial = moy[carac]
	else :
		return "Race ou caractéristique non supportée pour le moment"
	if lvl < initial :
		coeff = -1
		memo = initial
		initial = lvl
		lvl = memo
	#On renvoie la valeur totale
	diff = int(lvl)-initial
	result = coeff*(diff*(diff-1)*evo[carac]/2+diff*base[carac])
	if not silent :
		print "Cout pour un "+race+" d'obtenir "+str(lvl)+" en "+carac+" (lvl de base : "+str(initial)+") :"
	return result
def norm (carac) :
	if len(carac)<3 :
		carac+=" "*(3-len(carac))
	return carac

def compare(race1,race2,silent) :
	dict_race1 = eval(race1)
	dict_race2 = eval(race2)
	compt = 0
	for carac in dict_race1 :
		lvl = dict_race1[carac]
		cout_carac = cout(race2,carac,lvl,True)
		if not silent :
			print norm(carac)+" : passer de "+norm(str(lvl))+" ("+race1+") à "+norm(str(dict_race2[carac]))+" ("+race2+") : "+str(cout_carac)
		compt += cout_carac
	if not silent :
		print "Cout total pour un "+race2+" de rattraper le profil de base d'un "+race1+" :"
	return compt


def main() :
	if len(sys.argv) < 2 :
		print "Pas de fonction appelée"
		sys.exit(1)
	fonction = sys.argv[1]
	if fonction == "cout" :
		if len(sys.argv) < 5 :
			print "Nombre d'arguments invalides pour la fonction cout, 3 demandés"
			sys.exit(1)
		print cout(sys.argv[2],sys.argv[3],sys.argv[4],False)
	elif fonction == "compare" :
		if len(sys.argv) < 4 :
			print "Nombre d'arguments invalides pour la fonction compare, 2 demandés"
		print compare(sys.argv[2],sys.argv[3],False)

main()

